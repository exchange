Datenschutzbestimmungen
=======================

Letzte Aktualisierung: Februar 2025

Diese Datenschutzbestimmungen beschreiben die Richtlinien und Grundsätze der Taler Operations AG (nachfolgend **TOPS**)
mit Sitz in Biel/Bienne, Schweiz, in Bezug auf die Erfassung, Verarbeitung, Verwahrung und Weitergabe persönlicher Daten
von Besuchern unserer Webseiten sowie von Nutzern der Anwendungen und Produkte, die TOPS anbietet. Mit der Nutzung der
Webseiten, Anwendungen und Produkte willigen die Nutzer automatisch in die rechtliche Gültigkeit dieser
Datenschutzbestimmungen ein.

Sie finden Informationen zu unserer Unternehmung auf den Webseiten unter www.taler-ops.ch. Wenn Sie Fragen oder
Bedenken zu diesen Datenschutzbestimmungen haben, wenden Sie sich bitte an uns mit einer Nachricht an
privacy@taler-ops.net.


Grundsätze
----------

Das ausdrückliche Ziel des Bezahlsystems auf Grundlage von GNU Taler ist der Schutz der privaten Daten seiner Nutzer.
Den Grundsätzen "Privacy by Design" und "Privacy by Default" verpflichtet erhebt TOPS bei der Nutzung des
Taler-Zahlungsdiensts keine persönlichen Daten und erfragt auch für andere Zwecke keine personenbezogenen Daten oder
sonstige persönliche Informationen. Sie können daher darauf vertrauen, dass der Datenschutz von der ersten Nutzung an
grundsätzlich besteht (Privacy by Design) und auch weiterhin gewahrt bleibt, wenn Sie die Voreinstellungen unserer
Anwendungen beibehalten (Privacy by Default).

Für den Bezahlvorgang mit dem von TOPS bereitgestellten Zahlungsdienst werden keine personenbezogenen Daten der
Zahlenden benötigt und auch nicht erfasst. Es ist technisch nicht möglich, auf einzelne Personen unter den Zahlenden
schließen zu können. Weder Transaktionsnummern noch eingesetzte kryptografische Signaturen, die zur Autorisierung der
Transaktionen verwendet werden, ermöglichen Rückschlüsse auf die Personen, die eine Zahlung an Begünstigte auslösen.

Falls Nutzer mit uns in persönlichen Kontakt treten wollen, werden in Abhängigkeit des zur Kontaktaufnahme gewählten
Mediums Telefonnummern, Email-Adressen oder Postanschriften erfasst und nur zum Zweck und für die Dauer des
Informationsaustauschs verwahrt. Wenn Sie Verbesserungswünsche oder Fehler in Anwendungen (wie z.B. Taler-Wallets, Taler
Merchant Backend, Taler Cashier oder Taler Point-of-Sales-Apps) bzw. auf unseren Webseiten melden wollen, nutzen Sie
bitte hierfür unser Ticketsystem Mantis (https://bugs.gnunet.org). Dabei hinterlassen Sie eine Email-Adresse, damit wir
bei Bedarf mit Ihnen in Kontakt treten können. Auch im Fall von Supportleistungen müssen wir unter Umständen Ihre
Kontaktdaten wie z.B. Email-Adressen, Telefonnummern oder Postanschriften erfassen. TOPS gibt diese Informationen jedoch
nicht an Dritte weiter, es sei denn, dies sei unbedingt erforderlich, um Ihnen unsere Anwendungen und Produkte
bereitzustellen oder um staatliche oder überstaatliche Gesetze einzuhalten.


Einhaltung der Datenschutzgesetzgebung
--------------------------------------

Hinsichtlich der Beschaffung, Verarbeitung und Nutzung personenbezogener Daten von Nutzern verpflichtet sich TOPS, die
Bestimmungen der gültigen Datenschutzgesetzgebung einzuhalten. Personenbezogene Daten werden von uns nur im Rahmen
gesetzlicher oder regulatorischer Verpflichtungen erhoben, verarbeitet, aufbewahrt oder weitergegeben. TOPS stellt
sicher, dass die Daten im Einklang mit den Schweizerischen Datenschutzbestimmungen, insbesondere dem Schweizer
Datenschutzgesetz (DSG) und der Verordnung über den Datenschutz (DSV), behandelt werden.

Alle Systemdaten werden primär in der Schweiz gehostet, sind nur autorisiertem Personal zugänglich und werden auf
verschlüsselten Festplatten redundant verwahrt, d.h. mit Backup-Speicherung. Autorisiertes Personal wird von TOPS einer
Sicherheitsprüfung unterzogen.

Know-Your-Customer-Verfahren (KYC) zur Feststellung von wirtschaftlich Berechtigten werden gegebenenfalls durch
von TOPS beauftragte Dienstleister ausgeführt. Diese sind ebenfalls verpflichtet, die Daten nach Recht und Gesetz des
jeweiligen Staats bzw. des Gebiets, in dem der Zahlungsdienst angeboten wird, sicher zu verarbeiten und zu verwahren.

TOPS stellt Nutzern auf ausdrückliche Anfrage technischen Support zur Verfügung. An der Erbringung dieser
Dienstleistung können Dritte beteiligt sein, die Zugriff auf zur Kommunikation notwendige personenbezogene Daten
erhalten und ebenfalls zur Einhaltung der Datenschutzgesetzgebung verpflichtet sind.


Zweck der Datenverarbeitung
---------------------------

TOPS verarbeitet und verwahrt die zur Erbringung der Dienste unbedingt benötigten Daten und insbesondere
personenbezogene Daten nur, wenn dies aus gesetzlichen oder regulatorischen Gründen verlangt wird. Kontoführende Banken
sind gesetzlich zu Know-Your-Customer-Verfahren (KYC) verpflichtet, um in Kenntnis zu sein über die Eigentümer bzw.
wirtschaftlich Berechtigten der Bankkonten, die an den von TOPS betriebenen Zahlungsdienst zum Zweck der Aufladung von
Taler-Wallets überweisen, und der Bankkonten, an die der Zahlungsdienst zum Zweck der Bezahlung überweist. TOPS
verarbeitet und verwahrt lediglich die Kontonummern und Namen derjenigen Kontoeigentümer, die an den Zahlungsdienst
überweisen, sowie der Kontoeigentümer, an die der Zahlungsdienst überweist. TOPS erfährt darüber hinaus den
Abhebebetrag, der an den Zahlungsdienst zum Zweck der Aufladung von Taler-Wallets überwiesen wird, und die Höhe der
aggregierten Überweisungen an Empfängerkonten.

Die Überweisungen erfordern Konten bei Banken in der Schweiz. Zum Empfang von Zahlungen zwischen einzelnen Nutzern
(Peer-to-Peer-Zahlungen) benötigen diese eine Schweizer Mobiltelefonnummer, die TOPS zum Zweck der Bestätigung
des Besitzes einer Schweizer Telefonnummer via PIN per SMS verarbeitet und verwahrt.


Dauer der Datenverwahrung
-------------------------

Die Geschäftsbeziehung zwischen TOPS und Begünstigten (Händler, Betriebe, Verkäufer und sonstige regelmässige Empfänger
von Überweisungen des Zahlungsdiensts) wird auf eine unbestimmte Dauer abgeschlossen. Sollten für über 12 Monate keine
Transaktionen an die Begünstigen erfolgen, gilt die Geschäftsbeziehung automatisch als beendet. Die erfassten Daten
der Begünstigten verwahrt TOPS darüber hinausgehend für die Dauer, die gesetzliche oder regulatorische Bestimmungen
erfordern, z.B. Gesetze zur Bekämpfung der Geldwäsche (AML) und zur Feststellung wirtschaftlich Berechtigter.

Wenn Nutzern eine Abhebung an ein Taler-Wallet nicht gelingen sollte oder wenn das Wallet-Guthaben saldiert werden soll,
wird TOPS an das ursprüngliche Bankkonto zurücküberweisen. Die dafür nötigen Daten verwahrt TOPS ebenfalls für die
Dauer, die gesetzliche oder regulatorische Bestimmungen erfordern.


Weitergabe von Daten
--------------------

TOPS geht von der Annahme aus, dass Taler-Wallets grundsätzlich keine persönlichen Daten übertragen, zumindest nicht
ohne die bewusste und informierte vorherige Einwilligung der Nutzer. Wenn TOPS personenbezogene Daten weiterleitet,
dann nur die Namen von Kontoeigentümern, Bankkontonummern und die Höhe der Überweisungen an Empfängerkonten. Im
Bedarfsfall kann TOPS diese Daten an zuständige Behörden und Auditoren weitergeben. Begünstigte müssen davon ausgehen,
dass ihre personenbezogenen Daten aufgrund gesetzlicher oder regulatorischer Bestimmungen von TOPS verarbeitet, verwahrt
und weitergegeben werden, z.B. zur Erfüllung der Abgabenordnung und gesetzlicher Aufbewahrungspflichten.

Wenn Nutzer mit uns in persönlichen Kontakt treten oder Supportleistungen erhalten, behält sich TOPS das Recht vor,
dafür benötigte personenbezogene Daten an Angestellte, beauftragte Dienstleister oder bevollmächtigte Auftragnehmer
weiterzugeben.


Aktualisierung dieser Datenschutzbestimmungen
---------------------------------------------

TOPS kann diese Datenschutzbestimmungen jederzeit anpassen und aktualisieren, es ändert sich dann das Datum bei "Letzte
Aktualisierung". Aktualisierungen erfolgen beispielsweise infolge von Änderungen gesetzlicher oder regulatorischer
Bestimmungen oder aufgrund der branchenüblichen Entwicklung.

TOPS kann die Nutzer nicht direkt auf Aktualisierungen der Datenschutzbestimmungen aufmerksam machen, da grundsätzlich
keine persönlichen Kontaktdaten erfasst werden, durch die sich die Nutzer von Webseiten, Anwendungen und Produkten
gezielt informieren lassen. Taler-Wallets können Sie jedoch informieren, wenn sie eine Aktualisierung der
Datenschutzbestimmungen feststellen.

Bitte lesen Sie diese Datenschutzbestimmungen aufmerksam und wiederholt, um über ihre bestehenden Inhalte und eventuelle
Veränderungen informiert zu bleiben.

Diese Datenschutzbestimmungen und ihre Aktualisierungen sind kein Bestandteil eines Vertrags, sondern werden durch die
Nutzung unserer Webseiten, Anwendungen und Produkte stillschweigend angenommen. Wenn Sie Fragen oder Bedenken zu diesen
Datenschutzbestimmungen haben, wenden Sie sich bitte an uns mit einer Nachricht an privacy@taler-ops.net.


Anwendbares Recht und Gerichtsstand
-----------------------------------

Soweit gesetzlich zulässig unterstehen alle Rechtsbeziehungen zwischen TOPS und Nutzern ausschliesslich dem materiellen
schweizerischen Recht, unter Ausschluss von Kollisionsrecht und unter Ausschluss von Staatsverträgen. Unter dem
Vorbehalt von entgegenstehenden zwingenden gesetzlichen Bestimmungen ist Biel ausschliesslicher Gerichtsstand und
Erfüllungsort. Für Nutzer und Begünstigte mit einem Wohnsitz ausserhalb der Schweiz ist Biel sodann auch Betreibungsort.
