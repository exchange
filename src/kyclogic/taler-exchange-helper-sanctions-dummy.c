/*
   This file is part of TALER
   Copyright (C) 2025 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU Affero General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file taler-exchange-helper-sanctions-dummy.c
 * @brief sanction list evaluation simulator with a dummy implementation
 * @author Christian Grothoff
 * @defgroup request Request handling routines
 */
#include "platform.h"
#include <gnunet/gnunet_util_lib.h>
#include <jansson.h>
#include "taler_json_lib.h"
#include "taler_templating_lib.h"
#include "taler_util.h"


/**
 * The main function of the taler-exchange-helper-sanctions-dummy.
 *
 * @param argc number of arguments from the command line
 * @param argv command line arguments
 * @return 0 ok, non-zero on error
 */
int
main (int argc,
      char *const *argv)
{
  double confidence = 0.0;
  double match = 1.0;
  json_t *input;
  json_error_t err;

  while (NULL != (input = json_loadf (stdin,
                                      JSON_DISABLE_EOF_CHECK,
                                      &err)))
  {
    struct GNUNET_TIME_Timestamp expiration
      = GNUNET_TIME_absolute_to_timestamp (
          GNUNET_TIME_relative_to_absolute (GNUNET_TIME_UNIT_HOURS));

    if (! json_is_object (input))
    {
      json_decref (input);
      return 1;
    }
    json_decref (input);
    fprintf (stdout,
             "%f %f %llu dummy\n",
             match,
             confidence,
             (unsigned long long) GNUNET_TIME_timestamp_to_s (expiration));
    fflush (stdout);
  }
  return 0;
}


/* end of taler-exchange-helper-sanctions-dummy.c */
