/*
  This file is part of TALER
  Copyright (C) 2025 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file taler-exchange-httpd_aml-transfer-get.c
 * @brief Return information about exchange wire transfers
 * @author Christian Grothoff
 */
#include "platform.h"
#include <gnunet/gnunet_util_lib.h>
#include <jansson.h>
#include <microhttpd.h>
#include <pthread.h>
#include "taler_json_lib.h"
#include "taler_mhd_lib.h"
#include "taler_signatures.h"
#include "taler-exchange-httpd.h"
#include "taler_exchangedb_plugin.h"
#include "taler-exchange-httpd_aml-transfer-get.h"
#include "taler-exchange-httpd_metrics.h"

/**
 * Maximum number of transfers we return in one request.
 */
#define MAX_TRANSFERS 1024

/**
 * Return transfer data.
 *
 * @param cls closure
 * @param row_id current row in AML status table
 * @param payto_uri account involved with the wire transfer
 * @param execution_time when was the transfer made
 * @param amount wire amount of the transfer
 */
static void
record_cb (
  void *cls,
  uint64_t row_id,
  const char *payto_uri,
  struct GNUNET_TIME_Absolute execution_time,
  const struct TALER_Amount *amount)
{
  json_t *transfers = cls;

  GNUNET_assert (
    0 ==
    json_array_append_new (
      transfers,
      GNUNET_JSON_PACK (
        GNUNET_JSON_pack_string ("payto_uri",
                                 payto_uri),
        GNUNET_JSON_pack_int64 ("rowid",
                                row_id),
        GNUNET_JSON_pack_timestamp ("execution_time",
                                    GNUNET_TIME_absolute_to_timestamp (
                                      execution_time)),
        TALER_JSON_pack_amount ("amount",
                                amount)
        )));
}


/**
 * Handle HTTP request by AML officer for transfer data.
 *
 * @param rc request context
 * @param officer_pub the AML officer
 * @param is_debit false to return credit data, true to return debit data
 * @param args further arguments provided (should be empty)
 * @return MHD status
 */
static MHD_RESULT
aml_transfer_get (
  struct TEH_RequestContext *rc,
  const struct TALER_AmlOfficerPublicKeyP *officer_pub,
  bool is_debit,
  const char *const args[])
{
  int64_t limit = -20;
  uint64_t offset;
  struct TALER_Amount threshold;

  if (NULL != args[0])
  {
    GNUNET_break_op (0);
    return TALER_MHD_reply_with_error (
      rc->connection,
      MHD_HTTP_NOT_FOUND,
      TALER_EC_GENERIC_ENDPOINT_UNKNOWN,
      args[0]);
  }
  TALER_MHD_parse_request_snumber (rc->connection,
                                   "limit",
                                   &limit);
  if (limit > 0)
    offset = 0;
  else
    offset = INT64_MAX;
  TALER_MHD_parse_request_number (rc->connection,
                                  "offset",
                                  &offset);
  if (offset > INT64_MAX)
  {
    GNUNET_break_op (0); /* broken client */
    offset = INT64_MAX;
  }
  TALER_amount_set_zero (TEH_currency,
                         &threshold);
  TALER_MHD_parse_request_amount (rc->connection,
                                  "threshold",
                                  &threshold);
  {
    json_t *transfers;
    enum GNUNET_DB_QueryStatus qs;

    transfers = json_array ();
    GNUNET_assert (NULL != transfers);
    if (limit > MAX_TRANSFERS)
      limit = MAX_TRANSFERS;
    if (limit < -MAX_TRANSFERS)
      limit = -MAX_TRANSFERS;
    if (is_debit)
      qs = TEH_plugin->select_exchange_debit_transfers (
        TEH_plugin->cls,
        &threshold,
        offset,
        limit,
        &record_cb,
        transfers);
    else
      qs = TEH_plugin->select_exchange_credit_transfers (
        TEH_plugin->cls,
        &threshold,
        offset,
        limit,
        &record_cb,
        transfers);
    switch (qs)
    {
    case GNUNET_DB_STATUS_HARD_ERROR:
    case GNUNET_DB_STATUS_SOFT_ERROR:
      json_decref (transfers);
      GNUNET_break (0);
      return TALER_MHD_reply_with_error (
        rc->connection,
        MHD_HTTP_INTERNAL_SERVER_ERROR,
        TALER_EC_GENERIC_DB_FETCH_FAILED,
        (is_debit)
        ? "select_exchange_debit_transfers"
        : "select_exchange_credit_transfers");
    case GNUNET_DB_STATUS_SUCCESS_NO_RESULTS:
      json_decref (transfers);
      return TALER_MHD_reply_static (
        rc->connection,
        MHD_HTTP_NO_CONTENT,
        NULL,
        NULL,
        0);
    case GNUNET_DB_STATUS_SUCCESS_ONE_RESULT:
      break;
    }
    return TALER_MHD_REPLY_JSON_PACK (
      rc->connection,
      MHD_HTTP_OK,
      GNUNET_JSON_pack_array_steal ("transfers",
                                    transfers));
  }
}


MHD_RESULT
TEH_handler_aml_transfer_credit_get (
  struct TEH_RequestContext *rc,
  const struct TALER_AmlOfficerPublicKeyP *officer_pub,
  const char *const args[])
{
  return aml_transfer_get (rc,
                           officer_pub,
                           false,
                           args);
}


MHD_RESULT
TEH_handler_aml_transfer_debit_get (
  struct TEH_RequestContext *rc,
  const struct TALER_AmlOfficerPublicKeyP *officer_pub,
  const char *const args[])
{
  return aml_transfer_get (rc,
                           officer_pub,
                           true,
                           args);
}


/* end of taler-exchange-httpd_aml-decisions_get.c */
