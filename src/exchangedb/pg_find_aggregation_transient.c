/*
   This file is part of TALER
   Copyright (C) 2022, 2024, 2025 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file exchangedb/pg_find_aggregation_transient.c
 * @brief Implementation of the find_aggregation_transient function for Postgres
 * @author Christian Grothoff
 */
#include "platform.h"
#include "taler_error_codes.h"
#include "taler_dbevents.h"
#include "taler_pq_lib.h"
#include "pg_find_aggregation_transient.h"
#include "pg_helper.h"


enum GNUNET_DB_QueryStatus
TEH_PG_find_aggregation_transient (
  void *cls,
  const struct TALER_NormalizedPaytoHashP *h_payto,
  struct TALER_FullPayto *payto_uri,
  struct TALER_WireTransferIdentifierRawP *wtid,
  struct TALER_MerchantPublicKeyP *merchant_pub,
  struct TALER_Amount *total)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_auto_from_type (h_payto),
    GNUNET_PQ_query_param_end
  };
  struct GNUNET_PQ_ResultSpec rs[] = {
    GNUNET_PQ_result_spec_auto_from_type ("merchant_pub",
                                          merchant_pub),
    GNUNET_PQ_result_spec_auto_from_type ("wtid_raw",
                                          wtid),
    GNUNET_PQ_result_spec_string ("payto_uri",
                                  &payto_uri->full_payto),
    TALER_PQ_RESULT_SPEC_AMOUNT ("amount",
                                 total),
    GNUNET_PQ_result_spec_end
  };

  PREPARE (pg,
           "find_transient_aggregations",
           "SELECT"
           "  atr.amount"
           " ,atr.wtid_raw"
           " ,atr.merchant_pub"
           " ,wt.payto_uri"
           " FROM wire_targets wt"
           " JOIN aggregation_transient atr"
           "   USING (wire_target_h_payto)"
           " WHERE wt.h_normalized_payto=$1"
           " LIMIT 1;"); /* Note: there should really be only 1 match */
  return GNUNET_PQ_eval_prepared_singleton_select (
    pg->conn,
    "find_transient_aggregations",
    params,
    rs);
}
