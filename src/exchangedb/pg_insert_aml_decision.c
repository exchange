/*
   This file is part of TALER
   Copyright (C) 2022, 2023, 2024 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file exchangedb/pg_insert_aml_decision.c
 * @brief Implementation of the insert_aml_decision function for Postgres
 * @author Christian Grothoff
 */
#include "platform.h"
#include "taler_error_codes.h"
#include "taler_dbevents.h"
#include "taler_pq_lib.h"
#include "pg_insert_aml_decision.h"
#include "pg_helper.h"
#include <gnunet/gnunet_pq_lib.h>


enum GNUNET_DB_QueryStatus
TEH_PG_insert_aml_decision (
  void *cls,
  const struct TALER_FullPayto payto_uri,
  const struct TALER_NormalizedPaytoHashP *h_payto,
  struct GNUNET_TIME_Timestamp decision_time,
  struct GNUNET_TIME_Timestamp expiration_time,
  const json_t *properties,
  const json_t *new_rules,
  bool to_investigate,
  const char *new_measure_name,
  const json_t *jmeasures,
  const char *justification,
  const struct TALER_AmlOfficerPublicKeyP *decider_pub,
  const struct TALER_AmlOfficerSignatureP *decider_sig,
  size_t num_events,
  const char *events[static num_events],
  size_t enc_attributes_size,
  const void *enc_attributes,
  struct GNUNET_HashCode *attributes_hash,
  struct GNUNET_TIME_Timestamp attributes_expiration_time,
  bool *invalid_officer,
  bool *unknown_account,
  struct GNUNET_TIME_Timestamp *last_date,
  uint64_t *legitimization_measure_serial_id)
{
  struct PostgresClosure *pg = cls;
  struct TALER_KycCompletedEventP rep = {
    .header.size = htons (sizeof (rep)),
    .header.type = htons (TALER_DBEVENT_EXCHANGE_KYC_COMPLETED),
    .h_payto = *h_payto
  };
  struct TALER_FullPaytoHashP h_full_payto;
  char *notify_s
    = GNUNET_PQ_get_event_notify_channel (&rep.header);
  struct GNUNET_PQ_QueryParam params[] = {
    /* $1: in_payto_uri */
    NULL == payto_uri.full_payto
      ? GNUNET_PQ_query_param_null ()
      : GNUNET_PQ_query_param_string (payto_uri.full_payto),
    /* $2: in_h_normalized_payto */
    GNUNET_PQ_query_param_auto_from_type (h_payto),
    /* $3: in_h_full_payto */
    NULL == payto_uri.full_payto
      ? GNUNET_PQ_query_param_null ()
      : GNUNET_PQ_query_param_auto_from_type (&h_full_payto),
    /* $4: in_decision_time */
    GNUNET_PQ_query_param_timestamp (&decision_time),
    /* $5: in_expiration_time*/
    GNUNET_PQ_query_param_timestamp (&expiration_time),
    /* $6: in_properties */
    NULL != properties
      ? TALER_PQ_query_param_json (properties)
      : GNUNET_PQ_query_param_null (),
    /* $7: in_kyc_attributes_enc */
    NULL != enc_attributes
      ? GNUNET_PQ_query_param_fixed_size (enc_attributes,
                                          enc_attributes_size)
      : GNUNET_PQ_query_param_null (),
    /* $8: in_kyc_attributes_hash */
    NULL != attributes_hash
        ? GNUNET_PQ_query_param_auto_from_type (attributes_hash)
        : GNUNET_PQ_query_param_null (),
    /* $9: in_kyc_attributes_expiration */
    GNUNET_PQ_query_param_timestamp (&attributes_expiration_time),
    /* $10: in_new_rules */
    TALER_PQ_query_param_json (new_rules),
    /* $11: in_to_investigate */
    GNUNET_PQ_query_param_bool (to_investigate),
    /* $12: in_new_measure_name */
    NULL != new_measure_name
      ? GNUNET_PQ_query_param_string (new_measure_name)
      : GNUNET_PQ_query_param_null (),
    /* $13: in_jmeasures */
    NULL != jmeasures
      ? TALER_PQ_query_param_json (jmeasures)
      : GNUNET_PQ_query_param_null (),
    /* $14: in_justification */
    NULL != justification
      ? GNUNET_PQ_query_param_string (justification)
      : GNUNET_PQ_query_param_null (),
    /* $15: in_decider_pub */
    NULL != decider_pub
      ? GNUNET_PQ_query_param_auto_from_type (decider_pub)
      : GNUNET_PQ_query_param_null (),
    /* $16: in_decider_sig */
    NULL != decider_sig
      ? GNUNET_PQ_query_param_auto_from_type (decider_sig)
      : GNUNET_PQ_query_param_null (),
    /* $17: in_notify_s*/
    GNUNET_PQ_query_param_string (notify_s),
    /* $18: ina_events */
    GNUNET_PQ_query_param_array_ptrs_string (num_events,
                                             events,
                                             pg->conn),
    GNUNET_PQ_query_param_end
  };
  struct GNUNET_PQ_ResultSpec rs[] = {
    GNUNET_PQ_result_spec_bool ("out_invalid_officer",
                                invalid_officer),
    GNUNET_PQ_result_spec_bool ("out_account_unknown",
                                unknown_account),
    GNUNET_PQ_result_spec_timestamp ("out_last_date",
                                     last_date),
    GNUNET_PQ_result_spec_uint64 ("out_legitimization_measure_serial_id",
                                  legitimization_measure_serial_id),
    GNUNET_PQ_result_spec_end
  };
  enum GNUNET_DB_QueryStatus qs;

  GNUNET_assert ( ( (NULL == decider_pub) &&
                    (NULL == decider_sig) &&
                    (NULL == justification) ) ||
                  ( (NULL != decider_pub) &&
                    (NULL != decider_sig) &&
                    (NULL != justification) ) );

  if (NULL != payto_uri.full_payto)
    TALER_full_payto_hash (payto_uri,
                           &h_full_payto);
  PREPARE (pg,
           "do_insert_aml_decision",
           "SELECT"
           " out_invalid_officer"
           ",out_account_unknown"
           ",out_last_date"
           ",out_legitimization_measure_serial_id"
           " FROM exchange_do_insert_aml_decision"
           "($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18);");
  qs = GNUNET_PQ_eval_prepared_singleton_select (pg->conn,
                                                 "do_insert_aml_decision",
                                                 params,
                                                 rs);
  GNUNET_PQ_cleanup_query_params_closures (params);
  GNUNET_free (notify_s);
  GNUNET_PQ_event_do_poll (pg->conn);
  return qs;
}
