--
-- This file is part of TALER
-- Copyright (C) 2024 Taler Systems SA
--
-- TALER is free software; you can redistribute it and/or modify it under the
-- terms of the GNU General Public License as published by the Free Software
-- Foundation; either version 3, or (at your option) any later version.
--
-- TALER is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License along with
-- TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
--

CREATE FUNCTION alter_table_aml_history8()
RETURNS VOID
LANGUAGE plpgsql
AS $$
BEGIN
  EXECUTE FORMAT (
    'ALTER TABLE aml_history'
    ' ADD COLUMN kyc_attributes_hash BYTEA CHECK(LENGTH(kyc_attributes_hash)=64)'
    '   DEFAULT NULL'
    ',ADD COLUMN kyc_attributes_serial_id INT8'
    '   DEFAULT NULL'
    ';'
  );

  PERFORM comment_partitioned_column(
     'Hash of the new attributes inserted by the AML officer.'
    ,'kyc_attributes_hash'
    ,'aml_history'
    ,NULL
  );

  PERFORM comment_partitioned_column(
     'Attributes inserted by the AML officer.'
    ,'kyc_attributes_serial_id'
    ,'aml_history'
    ,NULL
  );
END $$;


-- We need a separate function for this, as we call create_table only once but need to add
-- those constraints to each partition which gets created
CREATE FUNCTION foreign_table_aml_history8()
RETURNS void
LANGUAGE plpgsql
AS $$
DECLARE
  table_name TEXT DEFAULT 'aml_history';
BEGIN
  EXECUTE FORMAT (
    'ALTER TABLE ' || table_name ||
    ' ADD CONSTRAINT ' || table_name || '_foreign_key_kyc_attributes'
    ' FOREIGN KEY (kyc_attributes_serial_id)'
    ' REFERENCES kyc_attributes (kyc_attributes_serial_id)');
END
$$;

INSERT INTO exchange_tables
    (name
    ,version
    ,action
    ,partitioned
    ,by_range)
  VALUES
    ('aml_history8'
    ,'exchange-0008'
    ,'alter'
    ,TRUE
    ,FALSE),
    ('aml_history8'
    ,'exchange-0008'
    ,'foreign'
    ,TRUE
    ,FALSE);
