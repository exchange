/*
   This file is part of TALER
   Copyright (C) 2022, 2025 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file exchangedb/pg_find_aggregation_transient.h
 * @brief implementation of the find_aggregation_transient function for Postgres
 * @author Christian Grothoff
 */
#ifndef PG_FIND_AGGREGATION_TRANSIENT_H
#define PG_FIND_AGGREGATION_TRANSIENT_H

#include "taler_util.h"
#include "taler_json_lib.h"
#include "taler_exchangedb_plugin.h"
/**
 * Find existing entry in the transient aggregation table.
 *
 * @param cls the @e cls of this struct with the plugin-specific state
 * @param h_payto destination of the wire transfer
 * @param[out] payto_uri corresponding payto URI, to be freed by caller
 * @param[out] wtid wire transfer identifier of transient aggregation
 * @param[out] merchant_pub public key of the merchant
 * @param[out] total amount aggregated so far
 * @return transaction status
 */
enum GNUNET_DB_QueryStatus
TEH_PG_find_aggregation_transient (
  void *cls,
  const struct TALER_NormalizedPaytoHashP *h_payto,
  struct TALER_FullPayto *payto_uri,
  struct TALER_WireTransferIdentifierRawP *wtid,
  struct TALER_MerchantPublicKeyP *merchant_pub,
  struct TALER_Amount *total);

#endif
