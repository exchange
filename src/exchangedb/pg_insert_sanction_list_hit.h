/*
   This file is part of TALER
   Copyright (C) 2025 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file exchangedb/pg_insert_sanction_list_hit.h
 * @brief implementation of the insert_sanction_list_hit function for Postgres
 * @author Christian Grothoff
 */
#ifndef PG_INSERT_SANCTION_LIST_HIT_H
#define PG_INSERT_SANCTION_LIST_HIT_H

#include "taler_util.h"
#include "taler_json_lib.h"
#include "taler_exchangedb_plugin.h"


/**
 * Update sanction list hit status of the given account.
 *
 * @param cls closure
 * @param h_payto account for which the hit is to be stored
 * @param to_investigate true to flag account for investigation,
 *        false to **preserve** existing status
 * @param new_rules new KYC rules to apply to the account, NULL to preserve
 *        existing rules
 * @param account_properties new account properties
 * @param expiration_time when does the sanction list entry expire?
 * @param num_events length of the @a events array
 * @param events array of KYC events to trigger
 * @return database transaction status
 */
enum GNUNET_DB_QueryStatus
TEH_PG_insert_sanction_list_hit (
  void *cls,
  const struct TALER_NormalizedPaytoHashP *h_payto,
  bool to_investigate,
  const json_t *new_rules,
  const json_t *account_properties,
  struct GNUNET_TIME_Timestamp expiration_time,
  unsigned int num_events,
  const char **events);

#endif
