/*
   This file is part of TALER
   Copyright (C) 2025 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file exchangedb/pg_select_exchange_credit_transfers.h
 * @brief implementation of the select_exchange_credit_transfers function for Postgres
 * @author Christian Grothoff
 */
#ifndef PG_SELECT_EXCHANGE_CREDIT_TRANSFERS_H
#define PG_SELECT_EXCHANGE_CREDIT_TRANSFERS_H

#include "taler_util.h"
#include "taler_json_lib.h"
#include "taler_exchangedb_plugin.h"


/**
 * Return AML-relevant wire transfer credit data.
 *
 * @param cls closure
 * @param threshold minimum wire amount to return data for
 * @param offset offset in table to filter by
 * @param limit maximum number of entries to return, negative for descending
 * @param cb function to call on each result
 * @param cb_cls closure to pass to @a cb
 * @return transaction status
 */
enum GNUNET_DB_QueryStatus
TEH_PG_select_exchange_credit_transfers (
  void *cls,
  const struct TALER_Amount *threshold,
  uint64_t offset,
  int64_t limit,
  TALER_EXCHANGEDB_AmlTransferCallback cb,
  void *cb_cls);

#endif
