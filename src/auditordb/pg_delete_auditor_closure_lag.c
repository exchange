/*
   This file is part of TALER
   Copyright (C) 2025 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file auditordb/pg_delete_auditor_closure_lag.c
 * @brief Implementation of the delete_auditor_closure_lag function for Postgres
 * @author Christian Grothoff
 */
#include "platform.h"
#include "taler_error_codes.h"
#include "taler_dbevents.h"
#include "taler_pq_lib.h"
#include "pg_delete_auditor_closure_lag.h"
#include "pg_helper.h"


enum GNUNET_DB_QueryStatus
TAH_PG_delete_auditor_closure_lag (
  void *cls,
  const struct TALER_Amount *amount,
  const struct TALER_WireTransferIdentifierRawP *wtid,
  struct TALER_FullPayto credit_account_uri)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    TALER_PQ_query_param_amount (pg->conn,
                                 amount),
    GNUNET_PQ_query_param_auto_from_type (wtid),
    GNUNET_PQ_query_param_string (credit_account_uri.full_payto),
    GNUNET_PQ_query_param_end
  };

  PREPARE (pg,
           "delete_auditor_closure_lag",
           "DELETE FROM auditor_closure_lags "
           " WHERE (amount).frac=($1::taler_amount).frac"
           "  AND (amount).val=($1::taler_amount).val"
           "  AND wtid=$2"
           "  AND account=$3;");
  return GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                             "delete_auditor_closure_lag",
                                             params);
}
