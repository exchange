/*
   This file is part of TALER
   Copyright (C) 2024 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file auditordb/pg_select_reserve_in_inconsistency.h
 * @brief implementation of the select_reserve_in_inconsistency function for Postgres
 * @author Christian Grothoff
 */
#ifndef PG_SELECT_RESERVE_IN_INCONSISTENCY_H
#define PG_SELECT_RESERVE_IN_INCONSISTENCY_H

#include "taler_util.h"
#include "taler_json_lib.h"
#include "taler_auditordb_plugin.h"


/**
 * Return any reserve incoming inconsistency associated with the
 * given @a bank_row_id.
 *
 * @param cls closure
 * @param bank_row_id row to select by
 * @param[out] dc details to return
 */
enum GNUNET_DB_QueryStatus
TAH_PG_select_reserve_in_inconsistency (
  void *cls,
  uint64_t bank_row_id,
  struct TALER_AUDITORDB_ReserveInInconsistency *dc);


#endif
