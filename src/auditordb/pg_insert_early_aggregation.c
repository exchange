/*
   This file is part of TALER
   Copyright (C) 2025 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file auditordb/pg_insert_early_aggregation.c
 * @brief Implementation of the insert_early_aggregation function for Postgres
 * @author Christian Grothoff
 */
#include "platform.h"
#include "taler_error_codes.h"
#include "taler_dbevents.h"
#include "taler_pq_lib.h"
#include "pg_insert_early_aggregation.h"
#include "pg_helper.h"


enum GNUNET_DB_QueryStatus
TAH_PG_insert_early_aggregation (
  void *cls,
  uint64_t batch_deposit_serial_id,
  uint64_t tracking_serial_id,
  const struct TALER_Amount *total_amount)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_uint64 (&batch_deposit_serial_id),
    GNUNET_PQ_query_param_uint64 (&tracking_serial_id),
    TALER_PQ_query_param_amount (pg->conn,
                                 total_amount),
    GNUNET_PQ_query_param_end
  };

  PREPARE (pg,
           "auditor_insert_early_aggregation",
           "INSERT INTO auditor_pending_deposits "
           "(batch_deposit_serial_id"
           ",tracking_serial_id"
           ",amount"
           ") VALUES ($1,$2,$3);");
  return GNUNET_PQ_eval_prepared_non_select (
    pg->conn,
    "auditor_insert_early_aggregation",
    params);
}
