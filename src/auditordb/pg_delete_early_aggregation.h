/*
   This file is part of TALER
   Copyright (C) 2024 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file auditordb/pg_delete_early_aggregation.h
 * @brief implementation of the delete_early_aggregation function for Postgres
 * @author Christian Grothoff
 */
#ifndef PG_DELETE_EARLY_AGGREGATION_H
#define PG_DELETE_EARLY_AGGREGATION_H

#include "taler_util.h"
#include "taler_json_lib.h"
#include "taler_auditordb_plugin.h"


/**
 * Delete a row from the early aggregation table.
 * Usually done when the expected aggregation
 * was finally detected.
 *
 * @param cls the @e cls of this struct with the plugin-specific state
 * @param batch_deposit_serial_id which entry to delete
 * @return transaction status code
 */
enum GNUNET_DB_QueryStatus
TAH_PG_delete_early_aggregation (
  void *cls,
  uint64_t batch_deposit_serial_id);


#endif
