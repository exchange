/*
   This file is part of TALER
   Copyright (C) 2025 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file auditordb/pg_delete_auditor_closure_lag.h
 * @brief implementation of the delete_auditor_closure_lag function for Postgres
 * @author Christian Grothoff
 */
#ifndef PG_DELETE_AUDITOR_CLOSURE_LAG_H
#define PG_DELETE_AUDITOR_CLOSURE_LAG_H

#include "taler_util.h"
#include "taler_json_lib.h"
#include "taler_auditordb_plugin.h"

/**
 * A previously missing wire transfer may have been found. Remove an alert
 * (if we raised one).
 *
 * @param cls plugin closure
 * @param amount wire transfer amount
 * @param wtid wire transfer subject
 * @param credit_account_uri destination account
 * @return #GNUNET_DB_STATUS_SUCCESS_NO_RESULTS if no matching alert was
 *   found
 */
enum GNUNET_DB_QueryStatus
TAH_PG_delete_auditor_closure_lag (
  void *cls,
  const struct TALER_Amount *amount,
  const struct TALER_WireTransferIdentifierRawP *wtid,
  struct TALER_FullPayto credit_account_uri);

#endif
