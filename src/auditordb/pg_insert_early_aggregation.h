/*
   This file is part of TALER
   Copyright (C) 2024 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file auditordb/pg_insert_early_aggregation.h
 * @brief implementation of the insert_early_aggregation function for Postgres
 * @author Christian Grothoff
 */
#ifndef PG_INSERT_EARLY_AGGREGATION_H
#define PG_INSERT_EARLY_AGGREGATION_H

#include "taler_util.h"
#include "taler_json_lib.h"
#include "taler_auditordb_plugin.h"


/**
 * Insert new row into the early aggregation table.  This can happen simply
 * because of when the taler-helper-auditor-transfer looks at which of the
 * two tables. If it is cleared in the next iteration, this is perfectly
 * expected.
 *
 * @param cls the @e cls of this struct with the plugin-specific state
 * @param batch_deposit_serial_id where in the batch deposit table are we
 * @param tracking_serial_id where in the tracking table are we
 * @param total_amount value of all missing deposits, including fees
 * @return transaction status code
 */
enum GNUNET_DB_QueryStatus
TAH_PG_insert_early_aggregation (
  void *cls,
  uint64_t batch_deposit_serial_id,
  uint64_t tracking_serial_id,
  const struct TALER_Amount *total_amount);

#endif
