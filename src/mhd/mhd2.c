/*
  This file is part of TALER
  Copyright (C) 2014-2025 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file mhd2.c
 * @brief MHD utility functions (used by the merchant backend)
 * @author Christian Grothoff
 */
#include "platform.h"
#include "taler_util.h"
#include "taler_mhd2_lib.h"


enum GNUNET_GenericReturnValue
TALER_MHD2_is_https (struct MHD_Request *request)
{
  const struct MHD_StringNullable *forwarded_proto;
  union MHD_RequestInfoFixedData ci;
  union MHD_DaemonInfoFixedData di;

  forwarded_proto = MHD_request_get_value (request,
                                           MHD_VK_HEADER,
                                           "X-Forwarded-Proto");
  if ( (NULL != forwarded_proto) &&
       (NULL != forwarded_proto->cstr) )
  {
    if (0 == strcasecmp (forwarded_proto->cstr,
                         "https"))
      return GNUNET_YES;
    if (0 == strcasecmp (forwarded_proto->cstr,
                         "http"))
      return GNUNET_NO;
    GNUNET_break (0);
    return GNUNET_SYSERR;
  }
  /* likely not reverse proxy, figure out if we are
     http by asking MHD */
  if (MHD_SC_OK !=
      MHD_request_get_info_fixed (request,
                                  MHD_REQUEST_INFO_FIXED_DAEMON,
                                  &ci))
  {
    GNUNET_break (0);
    return GNUNET_SYSERR;
  }
  if (MHD_SC_OK !=
      MHD_daemon_get_info_fixed (ci.v_daemon,
                                 MHD_DAEMON_INFO_FIXED_TLS_TYPE,
                                 &di))
  {
    GNUNET_break (0);
    return GNUNET_SYSERR;
  }
  if (MHD_TLS_BACKEND_NONE != di.v_tls_backend)
    return GNUNET_YES;
  return GNUNET_NO;
}


bool
TALER_MHD2_arg_to_yna (struct MHD_Request *request,
                       const char *arg,
                       enum TALER_EXCHANGE_YesNoAll default_val,
                       enum TALER_EXCHANGE_YesNoAll *yna)
{
  const struct MHD_StringNullable *nstr;

  nstr = MHD_request_get_value (request,
                                MHD_VK_GET_ARGUMENT,
                                arg);
  if ( (NULL == nstr) ||
       (NULL == nstr->cstr) )
  {
    *yna = default_val;
    return true;
  }
  if (0 == strcasecmp (nstr->cstr, "yes"))
  {
    *yna = TALER_EXCHANGE_YNA_YES;
    return true;
  }
  if (0 == strcasecmp (nstr->cstr, "no"))
  {
    *yna = TALER_EXCHANGE_YNA_NO;
    return true;
  }
  if (0 == strcasecmp (nstr->cstr, "all"))
  {
    *yna = TALER_EXCHANGE_YNA_ALL;
    return true;
  }
  return false;
}
